class PostsController < ApplicationController
  def index
    @posts = Post.all
    render json: @posts
  end

  def show
    @post = Post.find(params[:id])
    render json: @post
  end

  def create
    @response = Post.create(title: params[:title])
    render json: @response 
  end

  def update
    @post = Post.find(params[:id])
    @response = @post.update(title: params[:title])
    render json: @response
  end

  def destroy
    @post = Post.find(params[:id])
    @response = @post.destroy
    render json: @response
  end
end
